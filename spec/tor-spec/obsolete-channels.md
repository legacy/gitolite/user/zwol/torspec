# Obsolete channel handshakes

These handshake variants are no longer in use.
Channel initiators MUST NOT send them.
Relays MAY detect and reject them.

> If you are experienced with TLS,
> you will find some aspects of this handshake
> strange or obfuscated.
> Several historical factors led to its current state.
>
> First, before the development of
> [pluggable transports](../pt-spec/),
> Tor tried to avoid censorship by mimicking the behavior
> of a web client negotiating HTTPS with a web server.
> If we wanted a secure option that was not in common use,
> we had to hide our use of that option.
>
> Second, prior to the introduction of
> [TLS 1.3](https://datatracker.ietf.org/doc/html/rfc8446),
> many more aspects of the handshake
> (such as the number and nature of certificates sent by each party)
> were sent in the clear,
> and were easy to distinguish.
>
> Third, prior to the introduction of TLS 1.3,
> there was no good encrypted signalling mechanism
> that a client could use to declare
> how it wanted the rest of the TLS handshake to proceed.
> Thus, we wound up using the client's list
> of supported ciphersuites
> to send a signal about which variation of the handshake is in use.


### Version 1, or "certificates up front" {#v1-handshake}

With this obsolete handshake,
the responding relay proves ownership of an RSA identity (`KP_relayid_rsa`),
and the initiator also proves ownership of an RSA identity.

(If the initiator does not have an RSA identity to prove,
it invents one and throws it away afterwards.)

To select this handshake,
the initiator starts a TLS handshake
containing no ciphersuites other than these:

<a id="v1-ciphersuite-list"></a>
```text
     TLS_DHE_RSA_WITH_AES_256_CBC_SHA
     TLS_DHE_RSA_WITH_AES_128_CBC_SHA
     SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA
```

> Note that because of this list,
> it is impossible to use this obsolete handshake
> with TLS 1.3.


As part of the TLS handshake,
the initiator sends a two-certificate chain,
consisting of an X.509 certificate
for its short-term connection public key (`KP_legacy_conn_tls`)
signed by `KP_relayid_rsa`, and
a second self-signed X.509 certificate containing `KP_relayid_rsa`.
The responder sends a similar
certificate chain.

Once the TLS handshake is done,
both parties validate each other's certificate chains.
If they are valid,
then the connection is Open,
and both parties may start exchanging [cells](./cell-packet-format.md).

## Version 2, or "renegotiation" {#v2-handshake}

In "renegotiation" (a.k.a. "the v2 handshake"),
the connection initiator selects at least one ciphersuite
not in the [list above](#v1-ciphersuite-list).
The initiator sends no certificates, and
the responder sends a single connection certificate in return.

(If the responder sends a certificate chain,
the initiator assumes that it only knows about the v1 handshake.)

Once this initial TLS handshake is complete,
the initiator renegotiates the session.
During the renegotiation,
each party sends a two-certificate chain
as in the ["certificates up front"](#v1-handshake) handshake above.

When this handshake is used,
both parties immediately
send a VERSIONS cell, and after negotiating a link
protocol version (which will be 2), each sends a NETINFO cell
to confirm their addresses and timestamps.
At that point, the channel is Open.
No other intervening cell types are allowed.

## Indicating support for the in-protocol handshake

When the in-protocol handshake was new,
we placed a set of constraints on the certificate that the responder would send
to indicate that it supported the v3 handshake.

Specifically, if at least one of these properties
was true of the responders's certificate,
the initiator could be sure that the responder supported
the in-protocol handshake:

- The certificate is self-signed
- Some component other than "commonName" is set in the subject or
  issuer DN of the certificate.
- The commonName of the subject or issuer of the certificate ends
  with a suffix other than ".net".
- The certificate's public key modulus is longer than 1024 bits.

Otherwise, the initiator would assume
that only the v2 protocol was in use.

## Fixed ciphersuite list {#fixed-cipehrsuite-list}

For a long time, clients would advertise
a certain "fixed ciphersuite list"
regardless of whether they actually supported those ciphers.

That list is:

```text
     TLS1_ECDHE_ECDSA_WITH_AES_256_CBC_SHA
     TLS1_ECDHE_RSA_WITH_AES_256_CBC_SHA
     TLS1_DHE_RSA_WITH_AES_256_SHA
     TLS1_DHE_DSS_WITH_AES_256_SHA
     TLS1_ECDH_RSA_WITH_AES_256_CBC_SHA
     TLS1_ECDH_ECDSA_WITH_AES_256_CBC_SHA
     TLS1_RSA_WITH_AES_256_SHA
     TLS1_ECDHE_ECDSA_WITH_RC4_128_SHA
     TLS1_ECDHE_ECDSA_WITH_AES_128_CBC_SHA
     TLS1_ECDHE_RSA_WITH_RC4_128_SHA
     TLS1_ECDHE_RSA_WITH_AES_128_CBC_SHA
     TLS1_DHE_RSA_WITH_AES_128_SHA
     TLS1_DHE_DSS_WITH_AES_128_SHA
     TLS1_ECDH_RSA_WITH_RC4_128_SHA
     TLS1_ECDH_RSA_WITH_AES_128_CBC_SHA
     TLS1_ECDH_ECDSA_WITH_RC4_128_SHA
     TLS1_ECDH_ECDSA_WITH_AES_128_CBC_SHA
     SSL3_RSA_RC4_128_MD5
     SSL3_RSA_RC4_128_SHA
     TLS1_RSA_WITH_AES_128_SHA
     TLS1_ECDHE_ECDSA_WITH_DES_192_CBC3_SHA
     TLS1_ECDHE_RSA_WITH_DES_192_CBC3_SHA
     SSL3_EDH_RSA_DES_192_CBC3_SHA
     SSL3_EDH_DSS_DES_192_CBC3_SHA
     TLS1_ECDH_RSA_WITH_DES_192_CBC3_SHA
     TLS1_ECDH_ECDSA_WITH_DES_192_CBC3_SHA
     SSL3_RSA_FIPS_WITH_3DES_EDE_CBC_SHA
     SSL3_RSA_DES_192_CBC3_SHA
     [*] The "extended renegotiation is supported" ciphersuite, 0x00ff, is
         not counted when checking the list of ciphersuites.
```

When encountering this list,
a responder would not select any ciphersuites
besides the mandatory-to-implement
TLS_DHE_RSA_WITH_AES_256_CBC_SHA, TLS_DHE_RSA_WITH_AES_128_CBC_SHA,
and SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA.

Clients no longer report ciphers that they do not support.


## Legacy CERTS authentication: Responder has RSA Identity only {#certs-responder-legacy}

A Tor relay that has only an RSA identity key (`KP_relayid_rsa`)
and not an Ed25519 identity key (`KP_relayid_ed`)
will present a different set of certificates in its CERTS cell.

(Relays like this are no longer supported;
all relays must now have Ed25519 identities.)

To authenticate a responder as having only an RSA identity,
the initiator would verify the following:

- The CERTS cell contains exactly one CertType 2 `RSA_ID_X509` certificate.
  - This must be a self-signed certificate containing a 1024-bit RSA key;
    that key's exponent must be 65537.
    That key is `KP_relayid_rsa`.
- The CERTS cell contains exactly one CertType 1 `TLS_LINK_X509` certificate.
  - It must be signed with `KP_relayid_rsa`.
  - Its subject key must be the same
    as `KP_legacy_conn_tls`
    (the key used to negotiate the TLS connection).
- All of the certs above must be correctly signed,
  not expired,
  and not before their `validAfter` dates.

### Legacy CERTS authentication: Initiator has RSA Identity only {#certs-initiator-legacy}


As discussed in
["Validating an initiator's CERTS"](./negotiating-channels.md#validate-initiator-certs),
the initiator of the v3 handshake does not present a TLS certificate.

Therefore, to process an initiator's CERTS cell,
the responder would have to  procede as for a responder's certificates,
[as described above](#certs-responder-legacy),
except that **instead** of checking for a `TLS_LINK_X509` certificate,
it would need to verify that:

- The CERTS cell contains exactly one CertType 3
  `LINK_AUTH_X509` certificate.
  - This certificate must be signed with `KP_relayid_rsa`.
    Its subject key is deemed to be `KP_legacy_linkauth_rsa`.
- All of the certs above must be correctly signed,
  not expired,
  and not before their `validAfter` dates.


<a id="tor-spec.txt-4.4.1"></a>

## Link authentication type 1: RSA-SHA256-TLSSecret {#RSA-SHA256-TLSSecret}

This is an obsolete authentication method
used before RFC5705 support was ubiquitous.
It is nearly the same as
[Ed25519-SHA256-RFC5705](./negotiating-channels.md#Ed25519-SHA256-RFC5705),
but lacks support for Ed25519,
and does not use keying material exporters
(which were not widely supported at the time it as used.

If AuthType is `[00 01]` (meaning "RSA-SHA256-TLSSecret"),
then the authentication field of the AUTHENTICATE
cell contains the following:

| Field      | Size | Description |
| ---------  | ---- | ----------- |
| `TYPE`     | 8    | The nonterminated string `AUTH0001` |
| `CID`      | 32   | `SHA256(KP_relayid_rsa)` for initiator |
| `SID`      | 32   | `SHA256(KP_relayid_rsa)` for responder |
| `SLOG`     | 32   | SHA256 of responder transcript |
| `CLOG`     | 32   | SHA256 of initiator transcript |
| `SCERT`    | 32   | SHA256 of responder's TLS certificate |
| `TLSSECRETS`|32   | An ad-hoc HMAC output |
| `RAND`     | 24   | [Random bytes] |
| `SIG`      | Variable | RSA signature |


Notes are as for [Ed25519-SHA256-RFC5705],
except as follows:


- The `TLSSECRETS` fields holds a SHA256 HMAC,
  using the TLS master secret as the secret key,
  of the following concatenated fields:
    - `client_random`, as sent in the TLS Client Hello
    - `server_random`, as sent in the TLS Server Hello
    - the NUL terminated ASCII string:
        `"Tor V3 handshake TLS cross-certification"`
* The `SIG` fields holds an RSA signature of a SHA256 hash
  of all the previous fields
  (that is, `TYPE` through `RAND`),
  using the initiator's `KS_legacy_linkauth_rsa`.
  This field extends through the end of the AUTHENTICATE cell.

[Random bytes]: ./preliminaries.md#random-values
[Ed25519-SHA256-RFC5705]: ./negotiating-channels.md#Ed25519-SHA256-RFC5705

Responders MUST NOT accept this AuthType if the initiator has
claimed to have an Ed25519 identity.

