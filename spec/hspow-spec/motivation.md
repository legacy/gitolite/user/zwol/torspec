# Motivation

See the [denial-of-service overview](../dos-spec/overview.md) for the big-picture view.
Here we are focusing on a mitigation for attacks on one specific resource: onion service introductions.

Attackers can generate low-effort floods of introductions which cause the onion service and all involved relays to perform a disproportionate amount of work, leading to a denial-of-service opportunity.
This proof-of-work scheme intends to make introduction floods unattractive to attackers, reducing the network-wide impact of this activity.

Previous to this work, our attempts at limiting the impact of introduction flooding DoS attacks on onion services has been focused on horizontal scaling with Onionbalance, optimizing the CPU usage of Tor and applying rate limiting.
While these measures move the goalpost forward, a core problem with onion service DoS is that building rendezvous circuits is a costly procedure both for the service and for the network.

For more information on the limitations of rate-limiting when defending against DDoS, see [`draft-nygren-tls-client-puzzles-02`](https://www.ietf.org/archive/id/draft-nygren-tls-client-puzzles-02.txt).

If we ever hope to have truly reachable global onion services, we need to make it harder for attackers to overload the service with introduction requests.
This proposal achieves this by allowing onion services to specify an optional dynamic proof-of-work scheme that its clients need to participate in if they want to get served.

With the right parameters, this proof-of-work scheme acts as a gatekeeper to block amplification attacks by attackers while letting legitimate clients through.

## Related work {#related-work}

For a similar concept, see the three internet drafts that have been proposed for defending against TLS-based DDoS attacks using client puzzles:

- [`draft-nygren-tls-client-puzzles-02`](https://www.ietf.org/archive/id/draft-nygren-tls-client-puzzles-02.txt)
- [`draft-nir-tls-puzzles-00`](https://www.ietf.org/archive/id/draft-nir-tls-puzzles-00.txt)
- [`draft-ietf-ipsecme-ddos-protection-10`](https://tools.ietf.org/html/draft-ietf-ipsecme-ddos-protection-10)

## Threat model

### Attacker profiles {#attacker-profiles}

This mitigation is written to thwart specific attackers. The current protocol is not intended to defend against all and every DoS attack on the Internet, but there are adversary models we can defend against.

Let's start with some adversary profiles:

- "The script-kiddie"

  The script-kiddie has a single computer and pushes it to its limits.
  Perhaps it also has a VPS and a pwned server.
  We are talking about an attacker with total access to 10 GHz of CPU and 10 GB of RAM.
  We consider the total cost for this attacker to be zero $.

- "The small botnet"

  The small botnet is a bunch of computers lined up to do an introduction flooding attack.
  Assuming 500 medium-range computers, we are talking about an attacker with total access to 10 THz of CPU and 10 TB of RAM.
  We consider the upfront cost for this attacker to be about $400.

- "The large botnet"

  The large botnet is a serious operation with many thousands of computers organized to do this attack.
  Assuming 100k medium-range computers, we are talking about an attacker with total access to 200 THz of CPU and 200 TB of RAM.
  The upfront cost for this attacker is about $36k.

We hope that this proposal can help us defend against the script-kiddie attacker and small botnets.
To defend against a large botnet we would need more tools at our disposal (see the [discussion on future designs](./analysis-discussion.md#future-designs)).

### User profiles {#user-profiles}

We have attackers and we have users. Here are a few user profiles:

- "The standard web user"

  This is a standard laptop/desktop user who is trying to browse the web.
  They don't know how these defences work and they don't care to configure or tweak them.
  If the site doesn't load, they are gonna close their browser and be sad at Tor.
  They run a 2GHz computer with 4GB of RAM.

- "The motivated user"

  This is a user that really wants to reach their destination.
  They don't care about the journey; they just want to get there.
  They know what's going on; they are willing to make their computer do expensive multi-minute PoW computations to get where they want to be.

- "The mobile user"

  This is a motivated user on a mobile phone.
  Even tho they want to read the news article, they don't have much leeway on stressing their machine to do more computation.

We hope that this proposal will allow the motivated user to always connect where they want to connect to, and also give more chances to the other user groups to reach the destination.

### The DoS Catch-22 {#catch22}

This proposal is not perfect and it does not cover all the use cases.
Still, we think that by covering some use cases and giving reachability to the people who really need it, we will severely demotivate the attackers from continuing the DoS attacks and hence stop the DoS threat all together.
Furthermore, by increasing the cost to launch a DoS attack, a big class of DoS attackers will disappear from the map, since the expected ROI will decrease.
